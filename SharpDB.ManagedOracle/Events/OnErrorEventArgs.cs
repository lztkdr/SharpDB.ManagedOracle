﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace SharpDB
{
    /// <summary>
    /// 异常前 执行的事件。
    /// 意义/作用：在报异常前记录 执行的Sql命令参数 ，以及异常信息，类似于 飞机黑匣子。
    /// </summary>
    public class OnErrorEventArgs : EventArgs
    {
        /// <summary>
        /// 执行的多条Sql命令
        /// </summary>
        public string[] SqlCmds { get; private set; }

        /// <summary>
        /// 每条Sql命令对应的参数化数组
        /// </summary>
        public List<IDataParameter[]> LstParameters { get; private set; }

        /// <summary>
        /// 执行出错引发的异常
        /// </summary>
        public Exception Exception { get; private set; }

        /// <summary>
        /// BulkCopy 时的异常 记录 表名
        /// </summary>
        public string TableName { get; private set; }

        /// <summary>
        /// BulkCopy 时的异常 记录 字段对应关系
        /// </summary>
        public Dictionary<string, string> DictColumnMapping { get; private set; }

        public OnErrorEventArgs(string[] sqlCmds, object parameters, Exception exception, string tableName = null, object columnMappings = null)
        {
            this.SqlCmds = sqlCmds;
            if (parameters is DbParameterCollection)
            {
                DbParameterCollection paramColl = parameters as DbParameterCollection;
                LstParameters = new List<IDataParameter[]>();
                LstParameters.Add(GetParamArray(paramColl));
            }
            else if (parameters is List<IDataParameter>)
            {
                LstParameters = new List<IDataParameter[]>();
                List<IDataParameter> paramColl = parameters as List<IDataParameter>;
                LstParameters.Add(paramColl.ToArray());
            }
            else if (parameters is List<DbParameterCollection>)
            {
                List<DbParameterCollection> param = parameters as List<DbParameterCollection>;
                if (param != null && param.Count > 0)
                {
                    LstParameters = new List<IDataParameter[]>();
                    for (int x = 0; x < param.Count; x++)
                    {
                        DbParameterCollection coll = param[x];
                        LstParameters.Add(GetParamArray(param[x]));
                    }
                }
            }
            else if (parameters is DbParameterCollection[])
            {
                DbParameterCollection[] param = parameters as DbParameterCollection[];
                if (param != null && param.Length > 0)
                {
                    LstParameters = new List<IDataParameter[]>();
                    for (int x = 0; x < param.Length; x++)
                    {
                        DbParameterCollection coll = param[x];
                        LstParameters.Add(GetParamArray(param[x]));
                    }
                }
            }
            else if (parameters is List<IDataParameter[]>)
            {
                List<IDataParameter[]> param = parameters as List<IDataParameter[]>;
                this.LstParameters = param;
            }

            this.Exception = exception;

            this.TableName = tableName;

            if (columnMappings is Dictionary<string, string>)
            {
                this.DictColumnMapping = columnMappings as Dictionary<string, string>;
            }
            //else if (columnMappings is OracleBulkCopyColumnMappingCollection)
            //{
            //    this.DictColumnMapping = new Dictionary<string, string>();
            //    OracleBulkCopyColumnMappingCollection copyColMappings = columnMappings as OracleBulkCopyColumnMappingCollection;
            //    if (copyColMappings != null && copyColMappings.Count > 0)
            //    {
            //        for (int j = 0; j < copyColMappings.Count; j++)
            //        {
            //            var copyColMapping = copyColMappings[j];
            //            this.DictColumnMapping.Add(copyColMapping.SourceColumn, copyColMapping.DestinationColumn);
            //        }
            //    }
            //}
        }

        private static IDataParameter[] GetParamArray(DbParameterCollection coll)
        {
            if (coll != null && coll.Count > 0)
            {
                IDataParameter[] paramTemp = new IDataParameter[coll.Count];
                for (int j = 0; j < coll.Count; j++)
                {
                    paramTemp[j] = coll[j];
                }
                return paramTemp;
            }
            return null;
        }



    }
}
